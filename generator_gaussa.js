var canvas = document.getElementById('gauss');
var context = canvas.getContext('2d');
var centerX = canvas.width / 2;
var centerY = canvas.height / 2;
var radius = 10;

function gaussianRand() {
    var rand = 0;
  
    for (var i = 0; i < 6; i += 1) {
      rand += Math.random();
    }
  
    return rand / 6;
  }

function gaussianRandom(start, end) {
    return Math.floor(start + gaussianRand() * (end - start + 1));
  }
  context.beginPath();
  for (var i=0;i<100;i++){
      context.arc(gaussianRandom(Math.floor(Math.random()*100),Math.floor(Math.random()*100))+200,gaussianRandom(Math.floor(Math.random()*100)+150,Math.floor(Math.random()*100)), 0, radius, 2 * Math.PI, false);
      context.lineWidth = 1;
      context.strokeStyle = '#ff0000';
      context.stroke();
  } 