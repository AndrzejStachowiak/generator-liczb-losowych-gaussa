# Generator liczb losowych 

## Strona główna 

### Opis działania

Strona zawiera:

* `Tytuły`
* `Generatory` - rodzaje generatorów

## Prosty generator

### Opis działania

Użycie wbudowanej funkcji biblioteczki `Math` do generowania losowych punktów

## Generator Gaussa

### Opis działania

Skrypt w `js` generujący losowe punkty metodą Gaussa
